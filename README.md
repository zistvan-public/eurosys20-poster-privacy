# EuroSys20 Poster: In-Storage Data Transformations for Enforceable Privacy
By Claudiu Mihali, Anca Hangan, Gheorghe Sebestyen and Zsolt Istvan

### You can watch the one-minute pitch [here](https://gitlab.software.imdea.org/zistvan-public/eurosys20-poster-privacy/-/blob/master/presentation_video.mp4)

### You can read the extended abstract and see the poster [here](https://gitlab.software.imdea.org/zistvan-public/eurosys20-poster-privacy/-/blob/master/submission.pdf)

